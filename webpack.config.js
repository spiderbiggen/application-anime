// webpack.config.js
const HtmlWebpackPlugin = require('html-webpack-plugin');
const path = require('path');

module.exports = [
  {
    mode: 'development',
    entry: './src/electron.ts',
    target: 'electron-main',
    module: {
      rules: [{
        test: /\.ts$/,
        include: /src/,
        use: [{ loader: 'ts-loader' }],
      }],
    },
    output: {
      path: path.resolve(__dirname, 'dist'),
      filename: 'electron.js',
    },
  },
  {
    mode: 'development',
    entry: './src/react/index.tsx',
    target: 'electron-renderer',
    devtool: 'source-map',
    module: {
      rules: [
        {
          test: /\.ts(x?)$/,
          include: /src/,
          use: [{ loader: 'ts-loader' }],
        },
        {
          test: /\.(jpg|png)$/,
          include: /src/,
          use: {
            loader: 'url-loader',
            options: {
              limit: 25000,
            },
          },
        },
        {
          test: /\.svg$/,
          include: /src/,
          use: 'file-loader',
        },
        {
          test: /\.s?css$/,
          include: /src/,
          use: ['style-loader', 'css-loader', 'sass-loader'],
        },
      ],
    },
    output: {
      path: path.resolve(__dirname, 'dist'),
      filename: 'index.js',
    },
    resolve: {
      alias: {
        '@': path.resolve(__dirname, 'src'),
        Assets: path.resolve(__dirname, 'src/assets'),
        Components: path.resolve(__dirname, 'src/react/components'),
        Views: path.resolve(__dirname, 'src/react/views'),
      },
      extensions: ['.tsx', '.ts', '.js'],
    },
    plugins: [
      new HtmlWebpackPlugin({
        template: './src/assets/index.html',
      }),
    ],
  },
];
