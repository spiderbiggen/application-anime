import React from 'react';
import './Profile.scss';
import { connect } from 'react-redux';
import { AppState } from '@/store';
import { User } from '@/models';
import { Link } from 'react-router-dom';

interface ProfileProps {
  user: User | null
}

type Props = ProfileProps;

class Profile extends React.Component<Props> {

  render() {
    if (!this.props.user) {
      return (
          <div className="Profile">
            <Link to="/login">Login</Link>
          </div>
      );
    }
    return (
        <div className="Profile">
          <span>{ this.props.user.mal_username || this.props.user.username }</span>
          <img src={ this.props.user.avatar } className="App-logo" alt="logo" />
        </div>
    );
  }
}

const mapStateToProps = (state: AppState) => {
  return {
    user: state.user && state.user.user || null,
  };
};

export default connect(
    mapStateToProps,
)(Profile);
