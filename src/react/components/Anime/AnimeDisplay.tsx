import React from 'react';
import './AnimeDisplay.scss';
import { connect } from 'react-redux';
import { ThunkDispatch } from 'redux-thunk';
import { AppState } from '@/store';
import { login } from '@/store/user/actions';
import { Anime } from '@/models';

interface ProfileProps {
  anime: Anime
}

interface DispatchProps {
}

type Props = ProfileProps & DispatchProps;

class AnimeDisplay extends React.Component<Props> {

  image = (anime: Anime) => {
    if (anime.image) return (
        <img src={ anime.image } alt={ `Cover art for: ${ anime.title }` } />
    );
  };


  render() {
    const anime = this.props.anime;
    return (
        <section className="Anime">
          <div className="Anime-container">
            { this.image(anime) }
            <div className="Anime-info-container">
              <h1>{ anime.title_english || anime.title }</h1>
              <h2>{ anime.title_japanese }</h2>
              <p> { anime.short_synopsis || anime.synopsis } </p>
            </div>
          </div>
        </section>
    );
  }
}

export default AnimeDisplay;
