import React from 'react';
import './AnimeGrid.scss'
import { Anime } from '@/models';
import AnimeDisplay from 'Components/Anime/AnimeDisplay';

interface ProfileProps {
  anime: Anime[]
}

interface DispatchProps {
}

type Props = ProfileProps & DispatchProps;

class AnimeGrid extends React.Component<Props> {
  render() {
    return (
        <div className="Anime-grid">
          {
            this.props.anime.map((anime: Anime) => {
              return <AnimeDisplay anime={ anime } key={ anime.id } />;
            })
          }
        </div>
    );
  }
}

export default AnimeGrid;
