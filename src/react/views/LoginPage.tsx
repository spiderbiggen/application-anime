import React, { createRef, FormEvent } from 'react';
import './LoginPage.scss';
import { ThunkDispatch } from 'redux-thunk';
import { connect } from 'react-redux';
import { AppState } from '@/store';
import { login } from '@/store/user/actions';

interface AppProps {}

interface DispatchProps {
  login: (username: string, password: string) => void
}

type Props = AppProps & DispatchProps;

class TopAnimePage extends React.Component<Props> {
  private readonly email = createRef<HTMLInputElement>();
  private readonly password = createRef<HTMLInputElement>();

  constructor(props: Props) {
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit(event: FormEvent<HTMLFormElement>) {
    event.preventDefault();
    if (this.email.current && this.password.current) {
      this.props.login(this.email.current.value, this.password.current.value);
    }
  }


  render() {
    return (
        <main className="center-content">
          <form className="Login-form" onSubmit={ this.handleSubmit }>
            <label>
              <input
                  name="email" type="text" autoComplete="email"
                  ref={ this.email } placeholder="&nbsp;"
              />
              <span>Email</span>
            </label>
            <label>
              <input
                  name="password" type="password" autoComplete="current-password"
                  ref={ this.password } placeholder="&nbsp;"
              />
              <span>Password</span>
            </label>
            <button>Login</button>
          </form>
        </main>
    );
  }
}

const mapStateToProps = (state: AppState) => ({});

const mapDispatchToProps = (dispatch: ThunkDispatch<{}, {}, any>): DispatchProps => {
  return {
    login: async (email, password) => {
      await dispatch(login(email, password));
    },
  };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(TopAnimePage);
