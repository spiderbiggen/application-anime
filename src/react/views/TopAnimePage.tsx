import React from 'react';
import { ThunkDispatch } from 'redux-thunk';
import { connect } from 'react-redux';
import { getTopAnime } from '@/store/anime/actions';
import { AppState } from '@/store';
import { Anime } from '@/models';
import AnimeGrid from 'Components/Anime/AnimeGrid';

interface AppProps {
  anime: Anime[]
}

interface DispatchProps {
  topAnime: () => void
}

type Props = AppProps & DispatchProps;

class TopAnimePage extends React.Component<Props> {
  componentDidMount() {
    this.props.topAnime();
  }


  render() {
    return (
        <AnimeGrid anime={ this.props.anime } />
    );
  }
}

const mapStateToProps = (state: AppState) => {
  return {
    anime: state.anime.anime,
  };
};

const mapDispatchToProps = (dispatch: ThunkDispatch<{}, {}, any>): DispatchProps => {
  return {
    topAnime: async () => {
      await dispatch(getTopAnime());
    },
  };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(TopAnimePage);
