import './index.scss';
import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import * as serviceWorker from '../serviceWorker';
import App from './App';
import configureStore from '@/store';
import { Color, Titlebar } from 'custom-electron-titlebar';

const store = configureStore();

const Root = () => (
    <Provider store={ store }>
      <App />
    </Provider>
);
render(<Root />, document.getElementById('root'));

new Titlebar({
  backgroundColor: Color.fromHex('#282c34'),
});
// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
