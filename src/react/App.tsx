import React from 'react';
import './App.scss';
import { HashRouter as Router, Link, Route, Switch } from 'react-router-dom';
import logo from 'Assets/logo.svg';
import Profile from 'Components/Profile/Profile';
import TopAnimePage from 'Views/TopAnimePage';
import LoginPage from 'Views/LoginPage';

class App extends React.Component {
  render() {
    return (
        <div className="App">
          <Router>
            <header className="App-header">
              <img src={ logo } className="App-logo" alt="logo" />
              <Link to="/">Anime</Link>
              <div className="spacer" />
              <Profile />
            </header>
            <Switch>
              <Route path="/" exact>
                <TopAnimePage />
              </Route>
              <Route path="/login">
                <LoginPage />
              </Route>
            </Switch>
          </Router>
        </div>
    );
  }
}

export default App;
