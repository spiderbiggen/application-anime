// src/electron.js
import { app, BrowserWindow } from 'electron';
import windowStateKeeper from 'electron-window-state';

async function createWindow() {
  const mainWindowState = windowStateKeeper({
    defaultWidth: 1000,
    defaultHeight: 800,
  });

  // Create the browser window.
  const win = new BrowserWindow({
    x: mainWindowState.x,
    y: mainWindowState.y,
    width: mainWindowState.width,
    height: mainWindowState.height,
    frame: false,
    webPreferences: {
      nodeIntegration: true,
    },
  });
  mainWindowState.manage(win);

  // and load the index.html of the app.
  try {
    await win.loadFile('index.html');
  } catch (e) {
    console.log(e);
    process.exit(-1);
  }
}

app.on('ready', createWindow);
