import { applyMiddleware, combineReducers, createStore } from 'redux';
import thunkMiddleware from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';

import { userReducer } from './user/reducers';
import { animeReducer } from './anime/reducers';

const rootReducer = combineReducers({
  user: userReducer,
  anime: animeReducer,
});

export type AppState = ReturnType<typeof rootReducer>;

export default function configureStore() {
  const middlewares = [thunkMiddleware];
  const middleWareEnhancer = applyMiddleware(...middlewares);

  return createStore(
      rootReducer,
      composeWithDevTools(middleWareEnhancer),
  );
}
