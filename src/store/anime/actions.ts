import { ThunkAction } from 'redux-thunk';
import { AnimeActionTypes, TOP } from './types';
import HttpService from '@/services/HttpService';
import { ArrayResponse } from '@/services/ResponseModels';
import { Anime } from '@/models';

export function setTop(top: Anime[]): AnimeActionTypes {
  return {
    type: TOP,
    anime: top,
  };
}

export const getTopAnime = (): ThunkAction<Promise<void>, {}, {}, AnimeActionTypes> => async dispatch => {
  const { results } = await HttpService.get<ArrayResponse<Anime>>('anime/top');
  console.log(results);
  dispatch(setTop(results));
};
