import { Anime } from '@/models';

export interface AnimeState {
  anime: Anime[],
}

export const TOP = 'TOP';

interface TopAction {
  type: typeof TOP;
  anime: Anime[];
}

// interface UserAction {
//   type: typeof USER;
//   user: Anime;
// }

export type AnimeActionTypes = TopAction;
