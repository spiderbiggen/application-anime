import { AnimeActionTypes, AnimeState, TOP } from './types';

const initialState: AnimeState = {
  anime: [],
};

export function animeReducer(state = initialState, action: AnimeActionTypes) {
  switch (action.type) {
    case TOP:
      return {
        anime: action.anime,
      };
    default:
      return state;
  }
}
