import { ThunkAction } from 'redux-thunk';
import { TOKEN, USER, UserActionTypes } from './types';
import HttpService from '@/services/HttpService';
import { TokenResponse } from '@/services/ResponseModels';
import { User } from '@/models';

export function setToken(token: string): UserActionTypes {
  HttpService.setToken(token);
  return {
    type: TOKEN,
    token: token,
  };
}

export function setUser(user: User): UserActionTypes {
  return {
    type: USER,
    user: user,
  };
}

export const login = (email: string, password: string): ThunkAction<Promise<void>, {}, {}, UserActionTypes> => async dispatch => {
  const { token } = await HttpService.post<TokenResponse>('users/authenticate', { email, password });
  dispatch(setToken(token));
  await dispatch(getSelf());
};

export const getSelf = (): ThunkAction<Promise<void>, {}, {}, UserActionTypes> => async dispatch => {
  const user = await HttpService.get<User>('users/self');
  dispatch(setUser(user));
};
