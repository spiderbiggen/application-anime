import { User } from '@/models';

export interface UserState {
  user: User | null,
  token: string | null
}

export const TOKEN = 'TOKEN';
export const USER = 'USER';

interface TokenAction {
  type: typeof TOKEN;
  token: string;
}

interface UserAction {
  type: typeof USER;
  user: User;
}

export type UserActionTypes = TokenAction | UserAction;
