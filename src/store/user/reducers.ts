import { TOKEN, USER, UserActionTypes, UserState } from './types';

const initialState: UserState = {
  token: null,
  user: null,
};

export function userReducer(state = initialState, action: UserActionTypes) {
  switch (action.type) {
    case TOKEN:
      return {
        user: state.user,
        token: action.token,
      };
    case USER:
      return {
        user: action.user,
        token: state.token,
      };
    default:
      return state;
  }
}
