export interface KeyedEntity {
  id: string;
}

export interface DatedEntity extends KeyedEntity {
  updatedAt: Date;
  createdAt: Date;
}

export interface User extends DatedEntity {
  username: string;
  mal_username?: string;
  email: string;
  password?: string;
  roles?: string[];
  avatar: string;
}

export interface Anime extends DatedEntity {
  mal_id: number;
  title: string;
  title_english?: string;
  title_japanese?: string;
  synopsis?: string;
  short_synopsis?: string;
  image?: string;
  from?: Date;
  to?: Date;
  mal_url: string;
  // urls?: Url[];
  // episodes?: Episode[];
}
