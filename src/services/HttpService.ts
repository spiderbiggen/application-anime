import Axios, { AxiosInstance, AxiosRequestConfig } from 'axios';
import * as R from 'ramda';

class HttpService {
  private readonly instance: AxiosInstance;

  constructor() {
    this.instance = Axios.create({
      baseURL: 'http://127.0.0.1:3000/',
      headers: {
        'Content-Type': 'application/json',
      },
    });
    this.instance.interceptors.response.use(
        (response) => {
          if (response.data.status === 'success') {
            response.data = response.data.data;
          }
          return response;
        },
        error => {
          if (R.path(['response', 'status'], error) === 401) {
            console.error('Unauthorized:', error);
            // store.dispatch('user/logout');
          }
          throw error;
        },
    );
  }


  async head<T = any>(url: string, config?: AxiosRequestConfig): Promise<T> {
    return (await this.instance.head<T>(url, config)).data;
  }

  async get<T = any>(url: string, config?: AxiosRequestConfig): Promise<T> {
    return (await this.instance.get<T>(url, config)).data;
  }

  async post<T = any>(url: string, data?: any, config?: AxiosRequestConfig): Promise<T> {
    return (await this.instance.post<T>(url, data, config)).data;
  }

  async patch<T = any>(url: string, data?: any, config?: AxiosRequestConfig): Promise<T> {
    return (await this.instance.patch<T>(url, data, config)).data;
  }

  async put<T = any>(url: string, data?: any, config?: AxiosRequestConfig): Promise<T> {
    return (await this.instance.put<T>(url, data, config)).data;
  }

  async delete<T = any>(url: string, config?: AxiosRequestConfig): Promise<T> {
    return (await this.instance.delete<T>(url, config)).data;
  }

  setToken(token: string) {
    this.instance.defaults.headers.Authorization = `Bearer ${ token }`;
  }
}

export default new HttpService();
