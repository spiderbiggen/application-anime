export interface TokenResponse {
  token: string;
}

export interface ArrayResponse<T> {
  results: T[];
  count: number;
}
